## OpenDataFrom 氣象局 "臺北" 雨量觀測資料

### Requests
| method | url |
| ----------- | -------- |
| GET | `https://opendata.cwb.gov.tw/api/v1/rest/datastore/O-A0002-001` |

### Headers
|  key  |  description  |
| ----- |  -----------  |
|  Authorization  |  (Required) 氣象開放資料平台會員授權碼 |
|  locationName  |  (Array[String]) `["臺北"]` ( 測站代碼，請參考 [連結](https://e-service.cwb.gov.tw/wdps/obs/state.htm) )|
|  elementName  |  (Array[String]) `["MIN_10"]` ( 氣象因子，預設為全部回傳 )

-------
![架構圖](images/架構圖.png)

-----------

### How to run
- 1. Download Logstash binary  [link](https://www.elastic.co/downloads/logstash)
- 2. Start Elasticsearch、Kibana : 
```
cd weather_api_etl
docker-compose up -d
```
![docker-compose ps](images/docker_compose.png)

- 3. Start Logstash
``` 
screen -S logstash_screen
/path/to/bin/logstash -f /path/to/read_etl_info.conf 
ctrl a + d
```
![tracking log file](images/logstash.png)

- 4. Cronjob
```
crontab -e
*/10 * * * *  cd /path/to/weather_api_etl && /path/to/bin/python3 main.py
```
-----

### 監控資料收集的狀況
*Kibana*: 
```localhost:5601```
![監控](images/kibana.png)
---------

### Slack通知結果
![通知](images/slack-bot.png)