from weather_api import WeatherApi
from bigquery import BigqueryOperator
import json, os


def main():
    with open('config.json', 'r') as f:
        config = json.load(f)

    weather_config = config["weather_api"]
    bigquery_config = config["bigquery"]
    weather_api = WeatherApi()
    response = weather_api.requests_api(url=weather_config["base_url"], 
                                        payload=weather_config["payload"])
    records = weather_api.parse_response(response)
    bigquery_operator = BigqueryOperator(**bigquery_config, records=records)
    bigquery_operator.generate_client()
    bigquery_operator.insert_records()
    bigquery_operator.insert_histoy_records()


if __name__=='__main__':
    main()