from slack import SlackBot
from log import Logging
from google.cloud import bigquery
from google.oauth2 import service_account
from google.cloud.exceptions import NotFound
import sys, os, json


slack_bot = SlackBot()
logging = Logging(logger_name="bigquery", file_name="etl_info.log")
class BigqueryOperator:
    '''
    Connect to bigquery
    '''
    def __init__(self, project, location, dataset, table, records, file_name="records.txt"):
        self.project = project
        self.location = location
        self.dataset = dataset
        self.table = table
        self.table_id = '{}.{}.{}'.format(self.project, self.dataset, self.table)
        self.records = records
        self.file_name = file_name
        self.schema = [
            bigquery.SchemaField('lat', 'FLOAT', mode='NULLABLE'),
            bigquery.SchemaField('lon', 'FLOAT', mode='NULLABLE'),
            bigquery.SchemaField('location_name', 'STRING', mode='NULLABLE'),
            bigquery.SchemaField('station_id', 'STRING', mode='NULLABLE'),
            bigquery.SchemaField('obs_time', 'DATETIME', mode='NULLABLE'),
            bigquery.SchemaField('rain', 'FLOAT', mode='NULLABLE'), 
            bigquery.SchemaField('timezone', 'STRING', mode='NULLABLE')
        ]

    def generate_client(self):
        try:
            key_path = "/Users/jasonwu/vpon/homework/vpon-homework-aab1ab988654.json"
            credentials = service_account.Credentials.from_service_account_file(
                key_path, scopes=["https://www.googleapis.com/auth/cloud-platform"]
            )
            self.client = bigquery.Client(credentials=credentials, 
                                          project=self.project, 
                                          location=self.location)
        except Exception as err:
            msg = "{}".format(err)
            logging.error(info_type="client_connect", message=msg)
            self._write_record_file()
            slack_bot.send_message(msg)
            sys.exit(msg)

    def insert_records(self):
        """
        Check if table exists, if not then create table
        Insert records into bigquery
        Example: 
            records = [
                {
                    "lat": "25.0394", 
                    "lon": "121.5067", 
                    "location_name": "臺北", 
                    "station_id": "466920", 
                    "obs_time": "2021-04-24 16:00:00", 
                    "rain": 0.0, 
                    "timezone": "Asia/Taipei"
                }
            ]
        
        Raises:
            Exception: Unexpected error happend
        """
        try:
            self.client.get_table(self.table_id)

        except NotFound as err:
            self._create_table_if_not_exist()

        except Exception as err:
            msg = "{}".format(err)
            logging.error(info_type="insert_record", message=msg)
            self._write_record_file()
            slack_bot.send_message(msg)
            sys.exit(msg)

        try:
            self.client.insert_rows_json(self.table_id, self.records)
            logging.info(info_type="insert_record", message="insert bq success")
            slack_bot.send_message("Task: success")

        except Exception as err:
            msg = "{}".format(err)
            logging.error(info_type="insert_record", message=msg)
            self._write_record_file()
            slack_bot.send_message(msg)

        finally:
            self.client.close()
    
    def insert_histoy_records(self):
        if self._check_if_file_exists():
            try:
                old_records = self._read_record_file()
                self.client.insert_rows_json(self.table_id, old_records)
                self._delete_record_file()
                logging.info(info_type="insert_record", message="insert failed records success")

            except Exception as err:
                msg = "{}".format(err)
                logging.error(info_type="insert_record", message=msg)
            
        self.client.close()

    def _check_if_file_exists(self):
        return os.path.exists(self.file_name)

    def _write_record_file(self):
        """
        write records to local file
        """
        with open(self.file_name, "a") as f:
            json.dump(self.records[0], f)
            f.write("\n")

        msg = "write records into {}".format(self.file_name)
        logging.warning(info_type="write_file", message=msg)

    def _delete_record_file(self):
        os.remove(self.file_name)
        msg = "remove {}".format(self.file_name)
        logging.info(info_type="delete_file", message=msg)

    def _read_record_file(self):
        """
        Read record file
        """
        data = []
        with open(self.file_name) as f:
            for line in f:
                data.append(json.loads(line))
        return data

    def _create_table_if_not_exist(self):
        """
        If table not exist then create table
        Table: 
            Partition by {field: "obs_time", type: "MONTH"}
            clustering by {fields: ["obs_time"]}
        
        Raises:
            Exception: Unexpected error happend
        """
        table = bigquery.Table(self.table_id, schema=self.schema)
        table.time_partitioning = bigquery.TimePartitioning(
            type_=bigquery.TimePartitioningType.MONTH,
            field="obs_time"
        )
        table.clustering_fields = ["obs_time"]
        try:
            table = self.client.create_table(table)
            msg = "table not exists, create table: {}".format(self.table_id)
            logging.warning(info_type="create_table", message=msg)

        except Exception as err:
            msg = "{}".format(err)
            logging.error(info_type="create_table", message=msg)
            self._write_record_file()
            slack_bot.send_message(msg)
            sys.exit(msg)