import logging, json


class Logging:
    def __init__(self, logger_name, file_name):
        self.formatter = logging.Formatter(
            '{"timestamp": "%(asctime)s", "logger": "%(name)s", "level": "%(levelname)s", "message": [%(message)s]}', 
            datefmt="%Y-%m-%d %H:%M:%S"
        )
        self.logger = logging.getLogger(logger_name)
        self.logger.setLevel(logging.INFO)
        self.file_handler = logging.FileHandler(file_name)
        self.file_handler.setFormatter(self.formatter)
        self.logger.addHandler(self.file_handler)
    
    @staticmethod
    def _structured_msg(info_type, message, **kwargs):
        return '{"info_type":"%s", "msg":"%s", "extra":%s}' % (info_type, message, json.dumps(kwargs))
    
    def error(self, info_type, message, **kwargs):
        self.logger.error(self._structured_msg(info_type, message, **kwargs))
    
    def warning(self, info_type, message, **kwargs):
        self.logger.warning(self._structured_msg(info_type, message, **kwargs))

    def info(self, info_type, message, **kwargs):
        self.logger.info(self._structured_msg(info_type, message, **kwargs))