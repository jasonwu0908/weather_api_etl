from slack import SlackBot
from log import Logging
import requests
import sys


slack_bot = SlackBot()
logging = Logging(logger_name="weather_api", file_name="etl_info.log")
class WeatherApi:
    """
    requests & parse data from open weather data.
    response example: 
    {
        'success': 'true', 
        'result': {
            'resource_id': 'O-A0002-001', 
            'fields': [
                {'id': 'lat', 'type': 'Double'}, {'id': 'lon', 'type': 'Double'}, 
                {'id': 'locationName', 'type': 'String'}, {'id': 'stationId', 'type': 'String'}, 
                {'id': 'obsTime', 'type': 'Timestamp'}, {'id': 'elementName', 'type': 'String'}, 
                {'id': 'elementValue', 'type': 'String'}, {'id': 'parameterName', 'type': 'String'}, 
                {'id': 'parameterValue', 'type': 'String'}
                ]
        }, 
        'records': {
            'location': [
                {
                    'lat': '25.0394', 'lon': '121.5067', 
                    'locationName': '臺北', 'stationId': '466920', 
                    'time': {
                        'obsTime': '2021-04-25 15:20:00'
                        }, 
                    'weatherElement': [
                        {'elementName': 'MIN_10', 'elementValue': '-998.00'}
                    ], 
                    'parameter': [
                        {'parameterName': 'CITY', 'parameterValue': '臺北市'}, 
                        {'parameterName': 'CITY_SN', 'parameterValue': '01'}, 
                        {'parameterName': 'TOWN', 'parameterValue': '中正區'}, 
                        {'parameterName': 'TOWN_SN', 'parameterValue': '036'}, 
                        {'parameterName': 'ATTRIBUTE', 'parameterValue': '中央氣象局'}
                    ]
                }
            ]
        }
    }
    """
    def requests_api(self, url, payload):
        """
        Args:
            url: (String) opendata url
            payload: {
                "Authorization": "your-key", 
                "locationName": Array[String] (測站代碼), 
                "elementName": Array[String] (氣象因子)
            }

        Return:
            response: <Response [200]>

        Raises:
            HTTPError: Something went wrong with payload or url 
            Timeout: Waiting Slack response for too long time
            Exception: Unexpected error happend
        """
        try:
            response = requests.get(url=url, params=payload)
            response.raise_for_status()
            logging.info(info_type="requests", message="weather api requests success", status=response.status_code)
            return response
        
        except requests.exceptions.HTTPError as err:   
            msg = "{}".format(err)
            logging.error(info_type="requests", message=msg, status=response.status_code)
            slack_bot.send_message(msg)
            sys.exit(msg)
            
        except requests.exceptions.Timeout as err: 
            msg = "{}".format(err)
            logging.error(info_type="requests", message=msg, status=response.status_code)
            slack_bot.send_message(msg)
            sys.exit(msg)
        
        except Exception as err:
            msg = "{}".format(err)
            logging.error(info_type="requests", message=msg, status=response.status_code)
            slack_bot.send_message(msg)
            sys.exit(msg)
    
    def _get_rain_data(self, record):
        """
        Args:
            record: {
                'weatherElement': [
                    {
                        'elementName': 'MIN_10', 
                        'elementValue': '-998.00'
                    }
                ]
            }
        
        Return:
            rain: float or None

        Raises:
            KeyError: key not found
            ValueError: MIN_10 (string) can't convert to float
        """
        try:
            weather_elements = record["weatherElement"]
            rain_element = next((element for element in weather_elements if element["elementName"] == "MIN_10"), None)
        except KeyError as err:
            msg = "{} not found".format(err)
            logging.error(info_type="key_not_found", message=msg, key=err)
            slack_bot.send_message(msg)
            sys.exit(msg)

        if not rain_element:
            msg = "elementName: MIN_10 not found"
            logging.error(info_type="element_not_found", message=msg)
            slack_bot.send_message(msg)
            sys.exit(msg)
        
        rain = rain_element['elementValue']
        try:
            rain = float(rain)
        except ValueError as err:
            msg = "{}".format(err)
            logging.error(info_type="value_error", message=msg)
            slack_bot.send_message(msg)
            sys.exit(msg)
        
        if rain == float("-998.00"):
            return float(0)
        elif rain < 0:
            return None
        else:
            return rain

    def _parse_record(self, record):
        """
        Get specific data from record (lat, lon, locationName, stationId, obsTime, rain)
        
        Return: {
            "lat": ..., 
            "lon": ..., 
            "location_name": ..., 
            "station_id": ..., 
            "obs_time": ..., 
            "rain": ...
        }
        """
        try:
            lat = record["lat"]
            lon = record["lon"]
            location_name = record["locationName"]
            station_id = record["stationId"]
            obs_time = record["time"]["obsTime"]

        except KeyError as err:
            msg = "{} not found".format(err)
            logging.error(info_type="key_not_found", message=msg, key=err)
            slack_bot.send_message(msg)
            sys.exit(msg)

        rain = self._get_rain_data(record)
        data = {
            "lat": lat, 
            "lon": lon, 
            "location_name": location_name, 
            "station_id": station_id, 
            "obs_time": obs_time, 
            "rain": rain, 
            "timezone": "Asia/Taipei"
        }
        return data
        
    
    def parse_response(self, response):
        """
        Args:
            response: <Response [200]>

        Returns:
            data_list = [
                {
                    'lat': '25.0394', 'lon': '121.5067', 
                    'location_name': '臺北', 'station_id': '466920', 
                    'obs_time': '2021-04-25 15:20:00', 'rain': 0.0, 
                    'timezone': 'Asia/Taipei'
                }
            ]
            If a key is missing from the dictionary,
            then send message to slack and sys.exit.
        """
        data_list = []
        response = response.json()

        try:
            location = response["records"]["location"]
        except KeyError as err:
            msg = "{} not found".format(err)
            logging.error(info_type="key_not_found", message=msg, key=err)
            slack_bot.send_message(msg)
            sys.exit(msg)

        for record in location:
            data = self._parse_record(record)
            data_list.append(data)

        logging.info(info_type="parse_data", message="weather api parsed data success")
        return data_list
        