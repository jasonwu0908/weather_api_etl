from log import Logging
import requests
import json


logging = Logging(logger_name="slack_bot", file_name="etl_info.log")
class SlackBot:
    """
    Send message to slack channel
    """
    def __init__(self):
        self.url = "https://hooks.slack.com/services/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"
        self.headers = {"Content-type": "application/json"}
        self.chatbotname = "ETL_CHECK_BOT"
        
    def _generate_payload(self, message):
        payload = {
            "username": self.chatbotname, 
            "text": message
        }
        payload = json.dumps(payload)
        return payload

    def send_message(self, message):
        """
        Args:
            message: text message to slack channel
        
        Raises:
            HTTPError: Something went wrong with payload or url 
            Timeout: Waiting Slack response for too long time
            Exception: Unexpected error happend
        """
        payload = self._generate_payload(message)
        try:
            response = requests.post(url=self.url, data=payload, headers=self.headers)
            response.raise_for_status()

        except requests.exceptions.HTTPError as err:
            msg = "{}".format(err)
            logging.error(info_type="requests", message=msg)

        except requests.exceptions.Timeout as err: 
            msg = "{}".format(err)
            logging.error(info_type="requests", message=msg)

        except Exception as err:
            msg = "{}".format(err)
            logging.error(info_type="requests", message=msg)
